#you need to install requests: http://docs.python-requests.org/en/master/user/install/#install
import requests as rq;
import sys;

if len(sys.argv)<2:
	sys.exit("Missing username argument");
#.....change user id here....
user_id = sys.argv[1];

if len(sys.argv)<3:
	profile_name = None;
else:
	#profile name is not mandatory but it can help scraping accuracy
	profile_name = sys.argv[2];

#generate url
url = "https://facebook.com/"+user_id;

#load html string
rq_obj = rq.get(url);
html = rq_obj.text;

srckeyword = "src=\""; 

if profile_name == None:
	alt_text = "Profile Photo, Image may contain:";
else:
	alt_text = "alt=\""+profile_name+"&#039;s Profile Photo, Image may contain:";

pos_of_alt = (html.find(alt_text));
pos_of_keyword_url = html.find(srckeyword, (pos_of_alt+len(alt_text)));
pos_of_url_start = pos_of_keyword_url + len(srckeyword);
pos_of_url_end = html.find(" />", pos_of_url_start);

imgurl_raw = html[pos_of_url_start:(pos_of_url_end-1)];

#adjustment that makes the url work, UB
imgurl = imgurl_raw.replace('amp;', '');

print(imgurl);

